package ru.t1.gorodtsova.tm.service;

import ru.t1.gorodtsova.tm.api.repository.ITaskRepository;
import ru.t1.gorodtsova.tm.api.service.ITaskService;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.exception.entity.TaskNotFoundException;
import ru.t1.gorodtsova.tm.exception.field.DescriptionEmptyException;
import ru.t1.gorodtsova.tm.exception.field.NameEmptyException;
import ru.t1.gorodtsova.tm.model.Task;

import java.util.Collections;
import java.util.List;

public final class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = new Task();
        task.setName(name);
        add(task);
        return task;
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(task);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(projectId);
    }


    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(Integer index, Status status) {
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusById(String id, Status status) {
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}
